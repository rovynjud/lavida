package com.example.marou.incivisme;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificarFragment extends Fragment {

    public Button button;
    public TextView tvDireccio;
    public TextView tvLatitud;
    public TextView tvLongitud;
    public TextView tvDescripcio;
    public final int REQUEST_LOCATION_PERMISSION = 1;
    public FusedLocationProviderClient mFusedLocationClient;
    public ProgressBar mLoading;
    public boolean mTrackingLocation;
    public LocationCallback mLocationCallback;
    private SharedViewModel model;
    String mCurrentPhotoPath;
    private Uri photoURI;
    private ImageView foto;
    private Button buttonFoto;
    static final int REQUEST_TAKE_PHOTO = 1;
    private StorageReference storageRef;
    private String downloadUrl;

    public NotificarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notificar, container, false);
        mLoading = view.findViewById(R.id.loading);
        button = view.findViewById(R.id.button_notificar);
        tvDireccio = view.findViewById(R.id.txtDireccio);
        tvLatitud = view.findViewById(R.id.txtLatitud);
        tvLongitud = view.findViewById(R.id.txtLongitud);
        tvDescripcio = view.findViewById(R.id.txtDescripcio);
        foto = view.findViewById(R.id.foto);
        buttonFoto = view.findViewById(R.id.button_foto);


        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        model.getCurrentAddress().observe(this, address -> {
            tvDireccio.setText(getString(R.string.address_text,
                    address, System.currentTimeMillis()));
        });

        model.getCurrentLatLng().observe(this, latLng -> {
            tvLatitud.setText(Double.toString(latLng.latitude));
            tvLongitud.setText(Double.toString(latLng.longitude));
        });

        model.getButtonText().observe(this, s -> button.setText(s));
        model.getProgressBar().observe(this, visible -> {
            if(visible)
                mLoading.setVisibility(ProgressBar.VISIBLE);
            else
                mLoading.setVisibility(ProgressBar.INVISIBLE);
        });

        model.switchTrackingLocation();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

        button.setOnClickListener(button -> {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            storageRef = storage.getReference();

            StorageReference imageRef = storageRef.child(mCurrentPhotoPath);
            UploadTask uploadTask = imageRef.putFile(photoURI);

            uploadTask.addOnSuccessListener(taskSnapshot -> {
                imageRef.getDownloadUrl().addOnCompleteListener(task -> {
                    Uri downloadUri = task.getResult();
                    Glide.with(this).load(downloadUri).into(foto);

                    downloadUrl = downloadUri.toString();
                });
            });

            Incidencia incidencia = new Incidencia();
            incidencia.setDireccio(tvDireccio.getText().toString());
            incidencia.setLatitud(tvLatitud.getText().toString());
            incidencia.setLongitud(tvLongitud.getText().toString());
            incidencia.setProblema(tvDescripcio.getText().toString());
            incidencia.setUrl(downloadUrl);

            FirebaseAuth auth = FirebaseAuth.getInstance();
            DatabaseReference base = FirebaseDatabase.getInstance(
            ).getReference();

            DatabaseReference users = base.child("users");
            DatabaseReference uid = users.child(auth.getUid());
            DatabaseReference incidencies = uid.child("incidencies");
            DatabaseReference reference = incidencies.push();
            reference.setValue(incidencia);
        });

        buttonFoto.setOnClickListener(button -> {
            dispatchTakePictureIntent();
        });

        return view;
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(
                getContext().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(photoURI).into(foto);
            } else {
                Toast.makeText(getContext(),
                        "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
            }
        }
    }




}
